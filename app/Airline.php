<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    use ManyHandler;

    protected $fillable = [
        'title'
    ];

    public $timestamps = false;

    public function checkInformation()
    {
        return $this->hasMany(CheckInformation::class);
    }
}
