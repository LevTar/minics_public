<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Timezone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPUnit\Framework\Constraint\Count;

class CountriesController extends Controller
{
    private $validate = [
        'timezone_id' => 'required',
        'title' => 'required|max:50'
    ];
    private $route = [
        'index' => 'country.index',
        'edit' => 'country.edit'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = \App\Country::latest()->paginate(20);
        $timezones = Timezone::all();
        return view('dynamic-master', compact('countries', 'timezones'))->with(['page' => $this->route['index']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $country = $request->validate($this->validate);
        Country::create($country);
        return redirect()->route($this->route['index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        $timezones = Timezone::all();
        return view('dynamic-master', compact('country', 'timezones'))->with(['page' => $this->route['edit']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Country $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        $data = $request->validate($this->validate);
        $country->update($data);
        return redirect()->route($this->route['index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country $country
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Country $country)
    {
        $country->delete();
        return redirect()->route($this->route['index']);
    }
}
