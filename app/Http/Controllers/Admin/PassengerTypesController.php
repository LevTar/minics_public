<?php

namespace App\Http\Controllers\Admin;

use App\PassengerType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PassengerTypesController extends Controller
{
    private $validate = [
        'type' => 'required|unique:passenger_types'
    ];
    private $route = [
        'index' => 'passenger-type.index',
        'edit' => 'passenger-type.edit'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passengerTypes = PassengerType::latest()->paginate(20);
        return view('dynamic-master', compact('passengerTypes'))->with(['page' => $this->route['index']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $passengerType = $request->validate($this->validate);
        PassengerType::create($passengerType);
        return redirect()->route($this->route['index']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PassengerType  $passengerType
     * @return \Illuminate\Http\Response
     */
    public function show(PassengerType $passengerType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PassengerType  $passengerType
     * @return \Illuminate\Http\Response
     */
    public function edit(PassengerType $passengerType)
    {
        return view('dynamic-master', compact('passengerType'))->with(['page' => $this->route['edit']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PassengerType  $passengerType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PassengerType $passengerType)
    {
        $this->validate['type'] = $this->validate['type'] . ',type,' . $passengerType->id;
        $data = $request->validate($this->validate);
        $passengerType->update($data);
        return redirect()->route($this->route['index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PassengerType  $passengerType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PassengerType $passengerType)
    {
        $passengerType->delete();
        return redirect()->route($this->route['index']);
    }
}
