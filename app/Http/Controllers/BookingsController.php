<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Phonenumber;
use App\PassengerType;
use App\Hotel;
use Illuminate\Http\Request;

class BookingsController extends Controller
{
    private $route = [
        'index' => 'booking.index',
        'show' => 'booking.show'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $func = auth()->user()->role.'Index';
        return $this->$func();
    }

    private function adminIndex()
    {
        $bookings = Booking::latest()->paginate(20);
        return view('dynamic-master', compact('bookings'))->with(['page' => $this->route['index']]);
    }
    
    private function city_adminIndex()
    {
        $city = auth()->user()->city;
        $bookings = $city?Booking::whereIn('hotel_id', $city->hotel()->pluck('id')->toArray())->latest()->paginate(20):[];
        return view('dynamic-master', compact('bookings'))->with(['page' => $this->route['index']]);
    }

    private function lcIndex()
    {
        $user_id = auth()->user()->id;
        $hotel   = Hotel::where('lc_user_id', '=', $user_id)->first();
        $bookings = $hotel?$hotel->booking()->latest()->paginate(20):[];
        return view('dynamic-master', compact('bookings'))->with(['page' => $this->route['index']]);
    }

    private function repIndex()
    {
        return $this->city_adminIndex();
    }

    private function driverIndex()
    {
        return $this->city_adminIndex();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        $func = auth()->user()->role.'Show';
        return $this->$func($booking);
    }

    private function adminShow($booking)
    {
        return view('dynamic-master', compact('booking'))->with(['page' => $this->route['show']]);
    }

    private function city_adminShow($booking)
    {
        if(!$booking->hotel || ($booking->hotel && $booking->hotel->city_id != auth()->user()->city_id) && $booking->hotel->city_id)
            return redirect()->route($this->route['index']);
        return view('dynamic-master', compact('booking'))->with(['page' => $this->route['show']]);
    }

    private function lcShow($booking)
    {
        if(!$booking->hotel || ($booking->hotel && $booking->hotel->lc_user_id != auth()->user()->id))
            return redirect()->route($this->route['index']);
        $passenger_types = PassengerType::all();
        return view('dynamic-master', compact('booking', 'passenger_types'))->with(['page' => $this->route['show']]);
    }

    private function repShow($booking)
    {
        if(!$booking->hotel || ($booking->hotel && $booking->hotel->city_id != auth()->user()->city_id) && $booking->hotel->city_id)
            return redirect()->route($this->route['index']);
        $passenger_types = PassengerType::all();
        return view('dynamic-master', compact('booking', 'passenger_types'))->with(['page' => $this->route['show']]);
    }

    private function driverShow($booking)
    {
        return $this->city_adminShow($booking);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $func = auth()->user()->role.'Update';
        return $this->$func($request, $booking);
    }

    private function adminUpdate($request, $booking)
    {
    }

    private function city_adminUpdate($request, $booking)
    {
        $data = $request->validate(['pickup_time' => 'required|date_format:Y-m-d H:i']);

        $time_interval = strtotime($data['pickup_time']);
        $pickup_time = new \Datetime;
        $pickup_time->setTimestamp($time_interval);
        
        if (new \DateTime() > $pickup_time) {
            return back()->withError('Invalid date time!');
        }
        $booking->update(['pickup_time' => $pickup_time->format('Y-m-d H:i:s')]);
        
        return redirect()->route($this->route['show'], ['id'=>$booking->id]);
    }

    private function lcUpdate($request, $booking)
    {
        $validation = [
            'phonenumber' => 'nullable|numeric',
            'check_by_lc' => 'nullable',
            'passenger_type_id' => 'nullable|numeric'
        ];
        $data = $request->validate($validation);

        $passtype_id = !empty($data['passenger_type_id']) ? $data['passenger_type_id'] : \DB::raw('NULL');
        $booking->passenger->update(['type_id' => $passtype_id]);
        
        if($request->has('check_by_lc')) {
            if(empty($booking->check_by_lc))
                $booking->update(['check_by_lc' => \DB::raw('NOW()')]);
        }
        else{
            if(!empty($booking->check_by_lc))
                $booking->update(['check_by_lc' => \DB::raw('NULL')]);
        }
        
        $booking->passenger->phonenumber()->delete();
        if(!empty($data['phonenumber'])){
            $phonenumber = Phonenumber::firstOrCreate(['phonenumber' => $data['phonenumber']]);
            $booking->passenger->phonenumber()->attach(['phonenumber_id'=>$phonenumber->id]);
        }

        return redirect()->route($this->route['show'], ['id'=>$booking->id]);
    }

    private function repUpdate($request, $booking)
    {
        $validation = [
            'phonenumber'  => 'nullable|numeric',
            'check_by_rep' => 'nullable'
        ];
        $data = $request->validate($validation);
        
        if($request->has('check_by_rep')) {
            if(empty($booking->check_by_rep))
                $booking->update(['check_by_rep' => \DB::raw('NOW()')]);
        }
        else{
            if(!empty($booking->check_by_rep))
                $booking->update(['check_by_rep' => \DB::raw('NULL')]);
        }
        
        $booking->passenger->phonenumber()->delete();
        if(!empty($data['phonenumber'])){
            $phonenumber = Phonenumber::firstOrCreate(['phonenumber' => $data['phonenumber']]);
            $booking->passenger->phonenumber()->attach(['phonenumber_id'=>$phonenumber->id]);
        }

        return redirect()->route($this->route['show'], ['id'=>$booking->id]);
    }

    private function driverUpdate($request, $booking)
    {
        $request->validate(['no_show' => 'nullable']);
        
        if($request->has('no_show')) {
            if(empty($booking->no_show))
                $booking->update(['no_show' => \DB::raw('NOW()')]);
        }
        else{
            if(!empty($booking->no_show))
                $booking->update(['no_show' => \DB::raw('NULL')]);
        }
        
        return redirect()->route($this->route['show'], ['id'=>$booking->id]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
