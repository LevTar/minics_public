<?php

namespace App\Http\Controllers\city_admin;

use App\Hotel;
use App\City;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HotelsController extends Controller
{
    private $validate = [
        'title'   => 'required|unique:hotels',
        'lc_user_id' => 'nullable',
        'city_id' => 'required'
    ];
    private $route = [
        'index' => 'hotel.index',
        'edit'  => 'hotel.edit'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $func = auth()->user()->role.'Index';
        return $this->$func();
    }

    public function adminIndex()
    {
        $hotels = Hotel::latest()->paginate(20);
        $cities = City::all();
        return view('dynamic-master', compact('hotels', 'cities'))->with(['page' => $this->route['index']]);
    }

    public function city_adminIndex()
    {
        $city_id = auth()->user()->city_id;
        if(!$city_id)
            return redirect()->route('dashboard');
        $hotels = Hotel::where(['city_id'=>$city_id])->latest()->paginate(20);
        return view('dynamic-master', compact('hotels'))->with(['page' => $this->route['index']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $func = auth()->user()->role.'Store';
        return $this->$func($request);
    }

    private function adminStore($request)
    {
        $data = $request->validate($this->validate);
        $hotel = Hotel::create($data);
        return redirect()->route($this->route['edit'], ['id'=>$hotel->id]);
    }

    private function city_adminStore($request)
    {
        unset($this->validate['city_id']);
        $city_id = auth()->user()->city_id;
        if(!$city_id)
            return redirect()->route('dashboard');
        $data = $request->validate($this->validate);
        $data['city_id'] = auth()->user()->city_id;
        $hotel = Hotel::create($data);
        return redirect()->route($this->route['edit'], ['id'=>$hotel->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        $func = auth()->user()->role.'Edit';
        return $this->$func($hotel);
    }

    private function adminEdit($hotel)
    {
        $cities = City::all();
        $lcs    = User::where(['role' => 'lc', 'city_id' => $hotel->city_id])
                        ->whereNotIn('id', Hotel::where('id', '<>', $hotel->id)->whereNotNull('lc_user_id')->pluck('lc_user_id')->toArray())
                        ->get();
        return view('dynamic-master', compact('hotel', 'cities', 'lcs'))->with(['page' => $this->route['edit']]);
    }

    private function city_adminEdit($hotel)
    {
        $city_id = auth()->user()->city_id;
        if(!$city_id || $city_id != $hotel->city_id)
            return redirect()->route('dashboard');
        $lcs    = User::where(['role' => 'lc', 'city_id' => $city_id])
                        ->whereNotIn('id', Hotel::where('id', '<>', $hotel->id)->whereNotNull('lc_user_id')->pluck('lc_user_id')->toArray())
                        ->get();
        return view('dynamic-master', compact('hotel', 'lcs'))->with(['page' => $this->route['edit']]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        $func = auth()->user()->role.'Update';
        return $this->$func($request, $hotel);
    }

    private function adminUpdate($request, $hotel)
    {
        if($hotel->title==$request->input('title'))
            unset($this->validate['title']);// = $this->validate['title'] . ',title,' . $hotel->id;
        $data = $request->validate($this->validate);
        if($data['city_id'] != $hotel->city_id)
            $data['lc_user_id'] = null;
        $hotel->update($data);
        return redirect()->route($this->route['index']);
    }

    private function city_adminUpdate($request, $hotel)
    {
        unset($this->validate['city_id']);
        $city_id = auth()->user()->city_id;
        if(!$city_id || $city_id != $hotel->city_id)
            return redirect()->route('dashboard');
        $this->validate['title'] = $this->validate['title'] . ',title,' . $hotel->id;
        $data = $request->validate($this->validate);
        $hotel->update($data);
        return redirect()->route($this->route['index']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $func = auth()->user()->role.'Destroy';
        return $this->$func($hotel);
    }

    private function adminDestroy($hotel)
    {
        $hotel->delete();
        return redirect()->route($this->route['index']);
    }

    private function city_adminDestroy($hotel)
    {
        $city_id = auth()->user()->city_id;
        if(!$city_id || $hotel->city_id!=$city_id)
            return redirect()->route('dashboard');
        $hotel->delete();
        return redirect()->route($this->route['index']);
    }
}
