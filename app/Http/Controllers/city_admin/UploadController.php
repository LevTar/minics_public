<?php

namespace App\Http\Controllers\city_admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\ArrivalListLoader;
use App\TransferListLoader;

use Exception;

class UploadController extends Controller
{
    private $validate = [
        'city_id' => 'required'
    ];

    public function upload_data()
    {
        return view('dynamic-master', ['page'=>'upload-data', 'cities'=>City::all()]);
    }

    public function arrival_list(Request $request)
    {
        $request->validate($this->validate);
        try {
            $this->store_excel($request->file('arrival_list')->getRealPath(), new ArrivalListLoader, $this->getCity($request));
        } 
        catch(Exception $e) {
            return back()->withErrors($e->getMessage());
        }
        return redirect()->route('booking.index');
    }
    
    public function transfer_list(Request $request)
    {
        $request->validate($this->validate);
        try {
            $this->store_excel($request->file('transfer_list')->getRealPath(), new TransferListLoader, $this->getCity($request));
        } 
        catch(Exception $e) {
            return back()->withErrors($e->getMessage());
        }
        return redirect()->route('booking.index');
    }

    private function getCity(Request $request)
    {
        $city_id = intval($request->get('city_id'));
        if(auth()->user()->cannot('manage booking.city_id'))
            $city_id = auth()->user()->city_id;
        return $city_id;
    }

    private function store_excel($path, $list_loader, $city_id)
    {
        $list_loader->load($path, $city_id);
        $list_loader->storeToDB();
    }
}
