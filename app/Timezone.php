<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{

    protected $hidden = [
		'id',
		'created_at',
		'updated_at'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];

	protected $fillable = [
		'name',
		'abbr',
		'offset',
		'isdst',
		'text',
	];

	public static function allJSON()
	{
		$route = storage_path('time_zones.json');
		if(!file_exists($route))
			return [];
		return json_decode(file_get_contents($route), true);
	}

}
