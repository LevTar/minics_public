<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckInformation extends Model
{
    protected $fillable = [
        'date', 'time', 'airline_id', 'flight_no', 'supplier_id',
        'car_type_id', 'city_id', 'terminal_id'
    ];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function cartype()
    {
        return $this->belongsTo(CarType::class, 'car_type_id');
    }

    public function airline()
    {
        return $this->belongsTo(Airline::class, 'airline_id');
    }
}
