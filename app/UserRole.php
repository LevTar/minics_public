<?php

namespace App;

abstract class UserRole
{
    const ADMIN         = 'admin';
    const CITY_ADMIN    = 'city_admin';
    const REP           = 'rep';
    const LC            = 'lc';
    const DRIVER        = 'driver';
}
