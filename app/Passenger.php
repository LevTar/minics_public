<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    protected $guarded = [];

    public function fullname()
    {
        $fullname = '';
        foreach(['title', 'firstname', 'lastname'] as $field)
            $fullname .= empty($this->$field) ? '' : $this->$field . ' ';
        return $fullname;
    } 

    public function phoneNumber()
    {
        return $this->belongsToMany(Phonenumber::class, 'passenger_phonenumbers');
    }

    public function passengerType()
    {
        return $this->belongsTo(PassengerType::class, 'type_id');
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
