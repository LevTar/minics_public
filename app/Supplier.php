<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'title'
    ];


    public function checkInformation()
    {
        return $this->hasMany(CheckInformation::class);
    }
}
