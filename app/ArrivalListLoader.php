<?php

namespace App;

use App\ListLoader;

class ArrivalListLoader extends ListLoader
{
    protected $required_columns = [
        'booking id' => 1, 'hotel' => 1, 'leader name' => 1, 'supplier reference' => 1,
        'room type' => 1, 'no. of rooms' => 1, 'date & time' => 2, 'airline' => 2, 'flight no.' => 2
    ];
    protected function validate()
    {
        parent::validate();
        // ...
    }

    protected function instantiate()
    {
        parent::instantiate();
        $this->prepare();
    }

    protected function prepare()
    {
        $this->unsetAlreadySavedBookings();

        $this->checkin_date_times  = $this->getDateTimes('checkin');
        $this->checkout_date_times = $this->getDateTimes('checkout');
        
        $this->checkin_flight_no    = $this->getFlightNo('checkin');
        $this->checkout_flight_no   = $this->getFlightNo('checkout');
        
        $this->checkin_airlines     = $this->saveGetAirlines('checkin');
        $this->checkout_airlines    = $this->saveGetAirlines('checkout');

        $this->hotels               = $this->saveGetHotels();

        $this->room_types           = $this->saveGetRoomTypes();

        $this->passengers           = $this->saveGetPassengers();

    }

    protected function unsetAlreadySavedBookings()
    {
        $cindex     = $this->second_header_keys['booking id'];
        $bookings   = $this->getBookings();
        $bookings   = Booking::unsetIfSaved($bookings, 'booking_id');
        $result     = $this->result_array;
        for($i=2;!empty($result[$i]); $i++)
        {
            $key = trim($result[$i][$cindex]);
            if(!isset($bookings[$key])){
                $result[$i] = null;
                unset($result[$i]);
            }
        }
        $this->reOrganizeResult($result);
    }

    protected function reOrganizeResult($array)
    {
        $this->result_array = [];
        if(!empty($array)){
            foreach($array as $value)
                array_push($this->result_array, $value);
        }
    }

    public function storeToDB()
    {
        $booking_index  = $this->second_header_keys['booking id'];
        $supref_index   = $this->second_header_keys['supplier reference'];
        $norooms_index  = $this->second_header_keys['no. of rooms'];
        for($i=2; $i<count($this->result_array); $i++)
        {
            $res                        = $this->result_array[$i];
            $booking_id                 = trim($res[$booking_index]);
            $supref                     = trim($res[$supref_index]);
            $nofrooms                   = intval(trim($res[$norooms_index]));
            \DB::transaction(function() use ($i, $supref, $nofrooms, $booking_id){
                $checkin_array              = [];
                $checkout_array             = [];
                $booking                    = [];
                $index                      = $i-2;

                $booking['booking_id']      = $booking_id;
                $booking['passenger_id']    = $this->passengers[$booking_id];
                $booking['hotel_id']        = $this->hotels[$booking_id];
                if(strlen($supref))
                    $booking['supplier_ref']= $supref;
                if($nofrooms)
                    $booking['no_of_rooms'] = $nofrooms;
                                    
                $dtarr  = isset($this->checkin_date_times[$index])?
                                $this->checkin_date_times[$index]:[];
                $fno    = isset($this->checkin_flight_no[$index])?
                                $this->checkin_flight_no[$index]:'';
                $airl   = isset($this->checkin_airlines[$booking_id])?
                                $this->checkin_airlines[$booking_id]:0;
                $checkin_array = $this->checkinfoDBArray($dtarr, $fno, $airl);

                $booking['checkin_information_id']  = 
                    CheckInformation::create($checkin_array)->id;
                
                $dtarr  = isset($this->checkout_date_times[$index])?
                                $this->checkout_date_times[$index]:[];
                $fno    = isset($this->checkout_flight_no[$index])?
                                $this->checkout_flight_no[$index]:'';
                $airl   = isset($this->checkout_airlines[$booking_id])?
                                $this->checkout_airlines[$booking_id]:0;
                $checkout_array = $this->checkinfoDBArray($dtarr, $fno, $airl);

                $booking['checkout_information_id']  = 
                    CheckInformation::create($checkout_array)->id;
                    
                $booking_object = Booking::create($booking);

                $booking_object->roomType()->attach($this->room_types[$booking_id]);

            }, 3); // try adding three times
        }
    }

    protected function checkinfoDBArray($chinfo_dt, $chinfo_fno, $chinfo_airline)
    {
        $check_array                  = [];
        if(isset($chinfo_dt['date']))
            $check_array['date']      = $chinfo_dt['date'];

        if(isset($chinfo_dt['time']))
            $check_array['time']      = $chinfo_dt['time'];
        
        if(!empty($chinfo_fno))
            $check_array['flight_no'] = $chinfo_fno;

        if(!empty($chinfo_airline))
            $check_array['airline_id']= $chinfo_airline;

        return $check_array;
    }
}
    
