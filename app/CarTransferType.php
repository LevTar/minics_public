<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarTransferType extends Model
{
    protected $fillable = [
        'type'
    ];


    public function carType()
    {
        return $this->hasMany(CarType::class);
    }
}
