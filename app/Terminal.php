<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model
{
    protected $fillable = [
        'title', 'city_id'
    ];

    public function city()
    {
        return $this->belongsTo(Terminal::class, 'city_id');
    }
}
