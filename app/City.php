<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];


    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function timezone()
    {
        return $this->belongsTo(Timezone::class, 'timezone_id');
    }

    public function cityAbbr()
    {
        return $this->hasMany(CityAbbreviation::class);
    }

    public function hotel()
    {
        return $this->hasMany(Hotel::class);
    }
}
