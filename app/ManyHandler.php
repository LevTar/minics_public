<?php

namespace App;

trait ManyHandler
{
    public static function saveManyIfNotExists(array $arr, $other_fields=[])
    {
        $array = array();
        $field='';
        foreach($arr as $key => $value)
        {
            $field = $key;
            foreach($value as $v)
                $array[] = [$field => $v];
        }
        if(count($other_fields))
            foreach($other_fields as $key => $value)
                for($i=0;$i<count($array);$i++)
                    $array[$i][$key] = $value;
        return parent::getAndStore($array, $field);
    }

    public static function getAndStore($array, $field)
    {
        if(count($array))
        {
            $added = [];
            foreach($array as $value){
                $object = parent::firstOrCreate($value);
                // $added[$value[$field]] = $object->id;
                $added[] = $object->id;
            }
            return $added;
        }
        return [];
    }

    public static function unsetIfSaved($array, $field)
    {
        if(count($array))
        {
            foreach($array as $key => $value){
                $object = parent::where([$field => $value])->first();
                if(!empty($object)){
                    unset($array[$key]);
                }
            }
            return $array;
        }
        return [];
    }
}
