<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $guarded = [];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function timezone()
    {
        return $this->belongsTo(Timezone::class, 'timezone_id');
    }
}
