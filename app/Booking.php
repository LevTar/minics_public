<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use ManyHandler;

    protected $fillable = [
        'booking_id', 'passenger_id', 'hotel_id', 'supplier_ref', 'no_of_rooms',
        'check_by_rep', 'check_by_lc', 'no_show', 'pickup_time',
        'checkin_information_id', 'checkout_information_id', 'booking_date', 'no_of_passengers'
    ];

    public function passenger()
    {
        return $this->belongsTo(Passenger::class, 'passenger_id');
    }

    public function roomType()
    {
        return $this->belongsToMany(RoomType::class, 'booking_roomtype');
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }

    public function checkin()
    {
        return $this->belongsTo(CheckInformation::class, 'checkin_information_id');
    }

    public function checkout()
    {
        return $this->belongsTo(CheckInformation::class, 'checkout_information_id');
    }

}
