<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id', 30)->unique();
            $table->date('booking_date')->nullable();
            $table->integer('passenger_id')->unsigned();
            $table->foreign('passenger_id')->references('id')->on('passengers')->onUpdate('cascade');
            $table->integer('hotel_id')->unsigned()->nullable();
            $table->foreign('hotel_id')->references('id')->on('hotels')->onUpdate('cascade')->onDelete('set null');
            $table->string('supplier_ref', 40)->nullable();
            $table->dateTime('check_by_rep')->nullable();
            $table->dateTime('check_by_lc')->nullable();
            $table->dateTime('no_show')->nullable();
            $table->dateTime('pickup_time')->nullable();
            $table->tinyInteger('no_of_rooms')->nullable();
            $table->tinyInteger('no_of_passengers')->nullable();
            $table->integer('checkin_information_id')->unsigned()->nullable();
            $table->foreign('checkin_information_id')->references('id')->on('check_informations')->onUpdate('cascade')->onDelete('set null');
            $table->integer('checkout_information_id')->unsigned()->nullable();
            $table->foreign('checkout_information_id')->references('id')->on('check_informations')->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
        });
        Schema::create('booking_roomtype', function (Blueprint $table) {
            $table->integer('room_type_id')->unsigned();
            $table->foreign('room_type_id')->references('id')->on('room_types')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('booking_id')->unsigned();
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['room_type_id', 'booking_id']);
        });
        Schema::create('booking_roomnumber', function (Blueprint $table) {
            $table->integer('booking_id')->unsigned();
            $table->foreign('booking_id')->references('id')->on('bookings')->onUpdate('cascade');
            $table->integer('room_number')->unsigned();
            $table->primary(['booking_id', 'room_number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
