<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('title', ['Mr', 'Mrs', 'Ms', 'Dr'])->nullable();
            $table->string('firstname', 100);
            $table->string('lastname', 100);
            $table->integer('type_id')->unsigned()->nullable();
            $table->foreign('type_id')->references('id')->on('passenger_types')->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengers');
    }
}
