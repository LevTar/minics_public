<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonenumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phonenumbers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phonenumber', 20);
            $table->timestamps();
        });
        Schema::create('passenger_phonenumbers', function (Blueprint $table) {
            $table->integer('passenger_id')->unsigned();
            $table->foreign('passenger_id')->references('id')->on('passengers')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('phonenumber_id')->unsigned();
            $table->foreign('phonenumber_id')->references('id')->on('phonenumbers')->onDelete('cascade')->onUpdate('cascade');
            $table->primary(['passenger_id', 'phonenumber_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phonenumbers');
        Schema::dropIfExists('passenger_phonenumbers');
    }
}
