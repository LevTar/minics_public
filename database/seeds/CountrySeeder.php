<?php

use Illuminate\Database\Seeder;
use App\Country;
use App\Timezone;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create(['title'  => 'Iran', 'timezone_id'=>Timezone::where(['name'=>'Iran Standard Time'])->first()->id]);
        Country::create(['title'  => 'United Arab Emirates', 'timezone_id'=>Timezone::where(['name'=>'Arabian Standard Time'])->first()->id]);
    }
}
