<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TimezoneSeeder::class);

        $this->call(CountrySeeder::class);

        $this->call(CitySeeder::class);

        $this->call(UserSeeder::class);

        $this->call(RolesAndPermissionSeeder::class);
    }
}
