<?php
$dt = new \Datetime;
if(!empty($booking->pickup_time))
    $dt->setTimestamp(strtotime($booking->pickup_time));
$pickup_time = $dt->format('Y-m-d H:i');
?>
@extends('booking_show')
@section('edit_booking')
<div class="row">
    <form action="{{ route('booking.update',['id'=>$booking->id]) }}" class="ltr" method="post">
        @csrf
        {{ method_field('PATCH') }}

        @include('section.timepicker_field',
            ['name' => 'pickup_time', 'id' => 'pickup_time', 'persian_name' => 'زمان حضور مسافر',
            'value' => $pickup_time])

            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
    </form>
</div>
@stop