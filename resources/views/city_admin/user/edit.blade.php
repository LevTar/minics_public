@section('content')

    <div class="row">
        <form action="{{ route('user.update',['id'=>$user->id]) }}" method="post">
            @csrf
            {{ method_field('PATCH') }}

            <p style="color: red; direction: rtl">درصورت تغییر نقش مشاوران تفریحی، نقش آنها از هتل مربوطه حذف خواهد شد</p>

            @include('section.text_field', ['key' => 'firstname', 'id' => 'firstname', 'persian_key' => 'نام',
            'value' => $user->firstname, 'optional'=>true])

            @include('section.text_field', ['key' => 'lastname', 'id' => 'lastname', 'persian_key' => 'نام خانوادگی',
            'value' => $user->lastname, 'optional'=>true])

            @include('section.text_field', ['key' => 'username', 'id' => 'username', 'persian_key' => 'نام کاربری',
            'value' => $user->username])

            @include('section.text_field', ['key' => 'password', 'id' => 'password', 'persian_key' => 'رمز جدید',
            'value' => '', 'optional'=>true])

            @include('section.text_field', ['key' => 'phonenumber', 'id' => 'phonenumber', 'persian_key' => 'شماره تلفن',
            'value' => $phonenumber?$phonenumber->phonenumber:'', 'optional'=>true])

            <?php
            $role_objects = [];
            $roles_array = [\App\UserRole::LC, \App\UserRole::REP, \App\UserRole::DRIVER];
            foreach($roles_array as $role)
            {
                $role_object        = new StdClass;
                $role_object->id    = $role;
                $role_object->role  = $role;
                $role_objects[] = $role_object;
            }
            ?>
            @include('section.select_field',
                ['key' => 'role', 'id' => 'role', 'persian_key' => 'نقش',
                'objects' => $role_objects, 'opt_prop' => 'role', 'current_id' => $user->role])
            
            @include('section.submit_button', ['value'=>'ویرایش', 'btn_cls'=>'btn-info'])
        </form>
    </div>
@stop