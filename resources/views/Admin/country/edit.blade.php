@section('content')

    <div class="row">
        <form action="{{ route('country.update',['id'=>$country->id]) }}" class="ltr" method="post">
            @csrf
            {{ method_field('PATCH') }}
            @include('section.text_field', ['key' => 'title', 'id' => 'title', 'persian_key' => 'نام',
            'value' => $country->title])

            @include('section.select_field',
                ['key' => 'timezone_id', 'id' => 'timezone_id', 'persian_key' => 'منطقه زمانی',
                'objects' => $timezones, 'opt_prop' => 'text', 'current_id' => $country->timezone_id])
            
            @include('section.submit_button', ['value'=>'ویرایش', 'btn_cls'=>'btn-info'])
        </form>
    </div>
@stop