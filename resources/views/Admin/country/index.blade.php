@section('content')

    <div class="row">
        <form action="{{ route('country.store') }}" method="post">
            @csrf

            @include('section.text_field', ['key' => 'title', 'id' => 'title', 'persian_key' => 'نام',
            'value' => ''])

            @include('section.select_field',
                ['key' => 'timezone_id', 'id' => 'timezone_id', 'persian_key' => 'منطقه زمانی',
                'objects' => $timezones, 'opt_prop' => 'text', 'current_id' => -1])
            
            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
        </form>

    </div>
    <div class="row ltr">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>زمان</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($countries as $country)
                    <tr>
                        <td>{{ $country->title }}</td>
                        <td>{{ $country->timezone?$country->timezone->text:'' }}</td>
                        <td>
                            @include('section.deledit_form', [
                            'delroute' => route('country.destroy',['id'=>$country->id]),
                            'editroute' => route('country.edit',['id'=>$country->id])
                            ])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop