@section('content')

    <div class="row">
        <form action="{{ route('passenger-type.update',['id'=>$passengerType->id]) }}" class="ltr" method="post">
            @csrf
            {{ method_field('PATCH') }}
            @include('section.text_field', ['key' => 'type', 'id' => 'type', 'persian_key' => 'نوع',
            'value' => $passengerType->type])
            
            @include('section.submit_button', ['value'=>'ویرایش', 'btn_cls'=>'btn-info'])
        </form>
    </div>
@stop