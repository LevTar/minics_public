@section('content')

    <div class="row">
        <form action="{{ route('city.store') }}" method="post">
            @csrf
            @include('section.text_field', ['key' => 'title', 'id' => 'title', 'persian_key' => 'نام',
            'value' => ''])

            @include('section.select_field',
                ['key' => 'timezone_id', 'id' => 'timezone_id', 'persian_key' => 'منطقه زمانی',
                'objects' => $timezones, 'opt_prop' => 'text', 'current_id' => -1])
            
            @include('section.select_field',
                ['key' => 'country_id', 'id' => 'country_id', 'persian_key' => 'کشور',
                'objects' => $countries, 'opt_prop' => 'title', 'current_id' => -1])

            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
        </form>

    </div>
    <div class="row ltr">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
                <thead>
                <tr>
                    <th>نام</th>
                    <th>زمان</th>
                    <th>کشور</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cities as $city)
                    <tr>
                        <td>{{ $city->title }}</td>
                        <td>{{ $city->timezone?$city->timezone->text:'' }}</td>
                        <td>{{ $city->country?$city->country->title:'' }}</td>
                        <td>
                            @include('section.deledit_form', [
                                'delroute' => route('city.destroy',['id'=>$city->id]),
                                'editroute' => route('city.edit',['id'=>$city->id])
                                ])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop