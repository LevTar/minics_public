<div id="cityModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">انتخاب شهر</h4>
            </div>
            <div class="modal-body">
                <ul class='city_list'>
                @foreach($cities as $city)
                    <li class="city"><button class="styled" id="city_{{ $city->id }}" data-dismiss="modal">{{ $city->title }}</button></li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
