@section('content')

    <div class="row">
        <form action="{{ route('hotel.update',['id'=>$hotel->id]) }}" class="ltr" method="post">
            @csrf
            {{ method_field('PATCH') }}
            <p style="color: red; direction: rtl">تغییر شهر هتل باعث تغییر شهر تمام رزرو های مربوط به این هتل می شود</p>
            <p style="color: red; direction: rtl">همچنین تغییر شهر هتل، فیلد مشاوران تفریحی هتل مربوطه را بدون مقدار خواهد کرد</p>

            @include('section.text_field', ['key' => 'title', 'id' => 'title', 'persian_key' => 'عنوان',
            'value' => $hotel->title])

            @include('section.select_field',
                ['key' => 'city_id', 'id' => 'city_id', 'persian_key' => 'شهر',
                'objects' => $cities, 'opt_prop' => 'title', 'current_id' => $hotel->city_id])

            <div class="col-sm-6">
                <div class="form-group">
                    <label for="lc_user_id">مشاور تفریحی</label>
                    <select name="lc_user_id" id="lc_user_id" class="form-control">
                        <option value="">هیچکدام</option>
                        @foreach($lcs as $lc)
                            <option value="{{ $lc->id }}" {{ $hotel->lc_user_id == $lc->id ? 'selected' : '' }}>{{ $lc->username.'('.$lc->firstname.' '.$lc->lastname.')' }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            @include('section.submit_button', ['value'=>'ویرایش', 'btn_cls'=>'btn-info'])
        </form>
    </div>
@stop