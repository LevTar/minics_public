@section('content')

    <div class="row">
        <form action="{{ route('hotel.store') }}" method="post">
            @csrf

            @include('section.text_field', ['key' => 'title', 'id' => 'title', 'persian_key' => 'عنوان',
            'value' => ''])

            @include('section.select_field',
                ['key' => 'city_id', 'id' => 'city_id', 'persian_key' => 'شهر',
                'objects' => $cities, 'opt_prop' => 'title', 'current_id' => -1])


            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
        </form>

    </div>
    <div class="row ltr">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
                <thead>
                <tr>
                    <th>ردیف</th>
                    <th>عنوان</th>
                    <th>شهر</th>
                    <th>مشاور تفریحی مربوطه</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                <?php $index = 0; ?>
                @foreach($hotels as $hotel)
                    <tr>
                        <td>{{ ++$index }}</td>
                        <td>{{ $hotel->title }}</td>
                        <td>{{ $hotel->city?$hotel->city->title:'مشخص نشده' }}</td>
                        <td>{{ $hotel->lcuser?$hotel->lcuser->username.'('.$hotel->lcuser->firstname.' '.$hotel->lcuser->lastname.')':'مشخص نشده' }}</td>
                        <td>
                            @include('section.deledit_form', [
                            'delroute' => route('hotel.destroy',['id'=>$hotel->id]),
                            'editroute' => route('hotel.edit',['id'=>$hotel->id])
                            ])
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $hotels?$hotels->links():''}}
    </div>


@stop