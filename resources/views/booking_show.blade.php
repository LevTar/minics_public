@section('content')
<?php
    $passenger  = $booking->passenger;
    $phonenumber  = $passenger->phonenumber()->first();
    $passtype   = $passenger->passengerType;
    $hotel      = $booking->hotel;
    $city       = $hotel?$hotel->city:false;
    $checkin    = $booking->checkin;
    $checkin_airline    = $checkin->airline;
    $checkin_terminal    = $checkin->terminal;
    $checkin_supplier   = $checkin->supplier;
    $checkin_city       = $checkin->city;
    $checkin_cartype    = $checkin->cartype;
    $checkin_transfertype    = $checkin_cartype?$checkin_cartype->carTransferType:false;
    
    $checkout            = $booking->checkout;
    $checkout_airline    = $checkout->airline;
    $checkout_terminal   = $checkout->terminal;
    $checkout_supplier   = $checkout->supplier;
    $checkout_city       = $checkout->city;
    $checkout_cartype    = $checkout->cartype;
    $checkout_transfertype    = $checkout_cartype?$checkout_cartype->carTransferType:false;
    $data = [
        'کد رزرو'       => $booking->booking_id,
        'تاریخ رزرو'    => $booking->booking_date,
        'مسافر'         => $passenger?$passenger->fullname():'',
        'شماره تلفن مسافر'  => $phonenumber?$phonenumber->phonenumber:'',
        'نوع مسافر'     => $passtype?$passtype->type:'',
        'هتل'           => $hotel?$hotel->title:'',
        'شهر'           => $city?$city->title:'',
        'مرجع تامین کننده' => $booking->supplier_ref,
        'زمان تاییدیه مشاور تفریحی' => $booking->check_by_lc,
        'زمان تاییدیه نماینده' => $booking->check_by_rep,
        'زمان اعلام عدم حضور توسط راننده' => $booking->no_show,
        'زمان انتظار مسافر' => $booking->pickup_time,
        'تعداد اتاق ها' => $booking->no_of_rooms,
        'تعداد مسافران' => $booking->no_of_passengers,

        'تاریخ و زمان ورود' => $checkin->date . ' ' . $checkin->time,
        'خط هوایی ورود' => $checkin_airline?$checkin_airline->title:'',
        'تامین کننده ورود' => $checkin_supplier?$checkin_supplier->title:'',
        'شماره پرواز ورود' => $checkin->flight_no,
        'شهر مبدا' => $checkin_city?$checkin_city->title:'',
        'نوع وسیله حامل ورود' => $checkin_cartype?$checkin_cartype->type:'',
        'نوع انتقال ورود' => $checkin_transfertype?$checkin_transfertype->type:'',
        'پایانه ورود' => $checkin_terminal?$checkin_terminal->title:'',
        
        'تاریخ و زمان بازگشت' => $checkout->date . ' ' . $checkout->time,
        'خط هوایی بازگشت' => $checkout_airline?$checkout_airline->title:'',
        'تامین کننده بازگشت' => $checkout_supplier?$checkout_supplier->title:'',
        'شماره پرواز بازگشت' => $checkout->flight_no,
        'شهر بازگشت' => $checkout_city?$checkout_city->title:'',
        'نوع وسیله حامل بازگشت' => $checkout_cartype?$checkout_cartype->type:'',
        'نوع انتقال بازگشت' => $checkout_transfertype?$checkout_transfertype->type:'',
        'پایانه بازگشت' => $checkout_terminal?$checkout_terminal->title:'',

    ];
?>
@yield('edit_booking')
<div class="row">
    <div class="table-responsive">
        <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
            <thead>
            <tr>
                <th>خصیصه</th>
                <th>مقدار</th>
            </tr>
            </thead>
            <tbody>
                @foreach($data as $key => $value)
                <tr>
                    <td><strong>{{ $key }}</strong></td>
                    <td>{{ $value }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop