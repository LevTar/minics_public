<div id="topline"></div>
<div class="container">
    <div class="container-fluid">
        <div class="row">
            <div class="mini-submenu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </div>
            <div class="col-sm-4 col-md-3 sidebar">
                <div class="list-group">
                    <span class="pull-left" id="slide-submenu">
                        <span class="glyphicon glyphicon-remove-circle"></span>
                    </span>
                    @yield('panel_menu_items')
                </div>       
            </div> 
        </div>
    </div>
    <div id="panel-mid-content" class="pull-left col-sm-9">
