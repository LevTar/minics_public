<div class="col-sm-6">
    <div class="form-group">
        <label style="direction: rtl;" for="{{ $id }}">{{ $persian_name }}</label>
        <input type="text" id="{{ $id }}" class="timepicker form-control" name="{{ $name }}" value="{{ $value }}">
    </div>
</div>