@section('panel_menu_items')
<ul class="nav nav-sidebar">
    @foreach($panel_routing[auth()->user()->role] as $route => $fa)
        <li {{ explode('.', $page)[0]==explode('.', $route)[0]?' class=active':'' }}><a href="{{route($route)}}">{{ $fa }}</a></li>
    @endforeach
</ul>
<ul class="nav nav-sidebar ul-logout">
    <li><a href="{{route('logout')}}" class="btn btn-sm btn-danger" style="margin: 15px"><span class="glyphicon glyphicon-log-out"></span> خروج از پنل</a></li>
</ul>
@endsection