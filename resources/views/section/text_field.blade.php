<div class="col-sm-6">
    <div class="form-group">
        <label for="{{ $id }}">{{$persian_key}}</label>
        <input type="text" id="{{ $id }}" class="form-control" name="{{ $key }}" {{ empty($optional)?'required':'' }}
                value="{{ $value }}">
    </div>
</div>