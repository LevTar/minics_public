<div class="col-sm-6">
    <div class="form-group">
        <label for="{{ $id }}">{{ $persian_key }}</label>
        <select name="{{ $key }}" id="{{ $id }}" class="form-control">
            @if(!empty($optional))
                <option value="">{{ $optional }}</option>
            @endif
            @foreach($objects as $object)
                <option value="{{ $object->id }}" {{ $current_id == $object->id ? 'selected' : '' }}>{{ $object->$opt_prop }}</option>
            @endforeach
        </select>
    </div>
</div>
