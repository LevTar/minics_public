@section('content')
<h2>مدیریت مشاوران تفریحی</h2>
<form class="form-signin ajaxable" method="post" ajax-data-type="JSON" action="{{ route('admin.lc') }}">
    <br>
    {{ csrf_field() }}
    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
        <label for="inputEmail" class="sr-only">نام کاربری</label>
        <input type="username" name="username" id="inputEmail" class="form-control text-center" placeholder="نام کاربری"
                required autofocus>
        @if ($errors->has('username'))
            <span class="help-block">
            <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="inputPassword" class="sr-only">رمز عبور</label>
        <input name="password" type="text" id="inputPassword" class="form-control text-center"
                placeholder="رمز عبور"
                required>
        @if ($errors->has('password'))
            <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
        <label for="inputFirstname" class="sr-only">نام</label>
        <input name="firstname" type="text" id="inputFirstname" class="form-control text-center"
                placeholder="نام"
                required>
        @if ($errors->has('firstname'))
            <span class="help-block">
            <strong>{{ $errors->first('firstname') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
        <label for="inputLastname" class="sr-only">نام خانوادگی</label>
        <input name="lastname" type="text" id="inputLastname" class="form-control text-center"
                placeholder="نام خانوادگی"
                required>
        @if ($errors->has('lastname'))
            <span class="help-block">
            <strong>{{ $errors->first('lastname') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('phonenumber') ? ' has-error' : '' }}">
        <label for="inputPhonenumber" class="sr-only">شماره تلفن</label>
        <input name="phonenumber" type="text" id="inputPhonenumber" class="form-control text-center"
                placeholder="شماره تلفن"
                required>
        @if ($errors->has('phonenumber'))
            <span class="help-block">
            <strong>{{ $errors->first('phonenumber') }}</strong>
            </span>
        @endif
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">ثبت</button>
</form>
@endsection