<?php
$passenger  = $booking->passenger;
$ptype      = $passenger->passengerType;
$phonenumber= $passenger->phonenumber()->first();
?>
@extends('booking_show')
@section('edit_booking')
<div class="row">
    <form action="{{ route('booking.update',['id'=>$booking->id]) }}" class="ltr" method="post">
        @csrf
        {{ method_field('PATCH') }}


        @include('section.check_field',
            ['name' => 'check_by_lc', 'id' => 'check_by_lc', 'persian_name' => 'تایید حضور مسافر',
            'checked'=>!empty($booking->check_by_lc)])

        @include('section.select_field',
            ['key' => 'passenger_type_id', 'id' => 'passenger_type_id', 'persian_key' => 'نوع مسافر',
            'objects' => $passenger_types, 'optional'=>'انتخاب کنید', 'opt_prop' => 'type', 'current_id' => $ptype?$ptype->id:-1])

        @include('section.text_field',
            ['key' => 'phonenumber', 'id' => 'phonenumber', 'persian_key' => 'شماره تلفن مسافر',
            'optional'=>true, 'value' => $phonenumber?$phonenumber->phonenumber:''])

            @include('section.submit_button', ['value'=>'ثبت', 'btn_cls'=>'btn-info'])
    </form>
</div>
@stop