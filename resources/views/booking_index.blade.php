@section('content')
    <div class="row ltr">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-hover" style="border: 2px solid white">
                <thead>
                <tr>
                    <th>کد رزرو</th>
                    <th>هتل</th>
                    <th>مسافر</th>
                    <th>تاریخ رزرو</th>
                    <th>Driver No Show</th>
                    <th>LC Check Time</th>
                    <th>REP Check Time</th>
                    <th>Pickup Time</th>
                    <th>نوع مسافر</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bookings as $booking)
                <?php
                $no_show = empty($booking->no_show)?'':with(new \Datetime())->setTimestamp(strtotime($booking->no_show))->format('Y-m-d H:i');
                $check_by_lc = empty($booking->check_by_lc)?'':with(new \Datetime())->setTimestamp(strtotime($booking->check_by_lc))->format('Y-m-d H:i');
                $check_by_rep = empty($booking->check_by_rep)?'':with(new \Datetime())->setTimestamp(strtotime($booking->check_by_rep))->format('Y-m-d H:i');
                $pickup_time = empty($booking->pickup_time)?'':with(new \Datetime())->setTimestamp(strtotime($booking->pickup_time))->format('Y-m-d H:i');
                $passenger_type = $booking->passenger->passengerType?$booking->passenger->passengerType->type:'مشخص نشده';
                ?>
                    <tr>
                        <td><a href="{{route('booking.show',['id'=>$booking->id])}}">{{ $booking->booking_id }}</a></td>
                        <td>{{ $booking->hotel?$booking->hotel->title:'' }}</td>
                        <td>{{ $booking->passenger->fullname() }}</td>
                        <td>{{ $booking->booking_date }}</td>
                        <td>{{ $no_show }}</td>
                        <td>{{ $check_by_lc }}</td>
                        <td>{{ $check_by_rep }}</td>
                        <td>{{ $pickup_time }}</td>
                        <td>{{ $passenger_type }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $bookings?$bookings->links():'' }}
    </div>
@stop