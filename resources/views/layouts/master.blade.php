<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>پنل </title>
    <link rel="stylesheet" href="{{ mix('/css/admin.css') }}">
</head>
<body>
    @if ($errors->any())
    <div id="errors" style="display: none">
        {{ implode('، ', $errors->all(':message')) }}
    </div>
    @endif
    @include('section.header')
        @yield('content')
    @include('section.footer')
    <script src="{{ mix('/js/admin.js') }}"></script>
    <!-- include('sweet::alert') -->
</body>
</html>
