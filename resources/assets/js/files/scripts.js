/*
function login_callback(data, form)
{
    switch(data.result)
    {
        case 'error':
            swal ( "خطا!" ,  data.message ,  "error" );
            break;
        case 'captcha':
            document.location.href = data.url;
            break;
        case 'success':
            document.location.href = data.url;
            break;

    }
}
$('form.ajaxable').submit(false);
$('form.ajaxable').submit(function(e){
    let form = e.target;
    let url = form.getAttribute('ajax-url')?form.getAttribute('ajax-url'):form.getAttribute('action');
    let data_type = form.getAttribute('ajax-data-type')?form.getAttribute('ajax-data-type'):'JSON';
    let func_name = form.getAttribute('ajax-function');
    let data_array = $(form).serializeArray();
    $.post({
        url: url,
        dataType: data_type,
        data: data_array,
        success: function(data, textStatus, jqXHR) {
            if(typeof window[func_name] === "function")
                window[func_name](data, form);
            else
                console.log('Function "'+func_name+'" not found!');
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal ( "خطا!" ,  'خطا در دریافت اطلاعات! لطفا مجددا تلاش کنید.' ,  "error" );
            console.log(jqXHR);
        }
    });
});
*/

$(function(){

	$('#slide-submenu').on('click', function() {			        
        $(this).closest('.sidebar').fadeOut('slide',function(){
        	$('.mini-submenu').fadeIn();
            $('#panel-mid-content').css({width: "100%"});
        });
        
      });

	$('.mini-submenu').on('click', function(){		
        $(this).next('.sidebar').toggle('slide');
        $('.mini-submenu').hide();
        $('#panel-mid-content').css("width", "");
    });
    
    $('ul.city_list button').on('click', function(){
        $('input[name=city_id]').val($(this).attr('id').split('_')[1]);
        $('button#sel_city').html($(this).html());
    });

    var errors = $('div#errors').html();
    if(typeof errors !== "undefined")
        swal({
            type: 'error',
            title: 'خطا!',
            text: errors
        });
})
